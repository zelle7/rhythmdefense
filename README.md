# Rhythm Defense
The game was created during the GGJ Graz 2017.

http://globalgamejam.org/2017/games/rhythm-defense

## How to Play:
Rhythm Defense is a combination of Tower Defense and a Rhythm game.

### 1. Tower Defense:
You are allowed to place one musician per wave.
Click the musicians icon on the left of the map,
then click on the map to place it.
Place musicians strategically to target as many groupies as possible.

Your musicians are powered with energy from the rhythm game.

### 2. Rhythm Game:
Based on the songs rhythm red dots enter the screen.
Hit the space bar when a red dot enters the goal area
on the left to charge your musicians.


# QuickGDX

Based on the now deleted GDX Skeleton. This is a sample Project to quickly build new libgdx games.

## Basic functionality 
 - The project gives you 4 simple screens (menu, game, credits, loading). 
 - It gives you a simple tiles (tmx files) loading meachanism
 - It provides you simple collision detection
 - It implements a simple hud which can be extended
 - It implements a simple state mechanism which can be used inside your game logic
 
## Basic Setup
Clone the repo, add your code changes, build with gradle. Also look at https://github.com/libgdx/libgdx for further setup information.
 
## IntelliJ Idea Setup
Look at https://github.com/libgdx/libgdx/wiki/Gradle-and-Intellij-IDEA