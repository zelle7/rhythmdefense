package eu.quickgdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import eu.quickgdx.game.QuickGdx;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = QuickGdx.GAME_WIDTH;
        config.height = QuickGdx.GAME_HEIGHT;
		new LwjglApplication(new QuickGdx(), config);

	}
}
