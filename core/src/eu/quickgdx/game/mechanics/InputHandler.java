package eu.quickgdx.game.mechanics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import eu.quickgdx.game.mechanics.entities.GameObject;
import eu.quickgdx.game.mechanics.entities.ITouchable;

/**
 * Created by lknoch on 21.01.17.
 */
public class InputHandler {
    private World world;

    public InputHandler(World world) {
        this.world = world;
    }

    public void update(float delta){


        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            world.soundHud.tapped();
        }

        if (Gdx.input.justTouched()) {
            Vector3 unproject = world.gameplayScreen.gameCam.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 1));
            Vector2 touchingThereVeryGently = new Vector2(unproject.x, unproject.y);
            for (GameObject o : world.gameObjects) {
                if(o instanceof ITouchable){
                    if(((ITouchable) o).touch(touchingThereVeryGently)){
                        return;
                    }
                }
            }
            world.soundHud.tapped();
        }
    }
}
