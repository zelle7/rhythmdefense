package eu.quickgdx.game.mechanics;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.List;

import eu.quickgdx.game.QuickGdx;
import eu.quickgdx.game.ScreenManager;
import eu.quickgdx.game.mechanics.entities.BuildAbleObject;
import eu.quickgdx.game.mechanics.entities.CollisionObject;
import eu.quickgdx.game.mechanics.entities.CreepGoalObject;
import eu.quickgdx.game.mechanics.entities.CreepObject;
import eu.quickgdx.game.mechanics.entities.GameObject;
import eu.quickgdx.game.mechanics.entities.PlayerStatusObject;
import eu.quickgdx.game.mechanics.entities.Stats;
import eu.quickgdx.game.mechanics.entities.TowerObject;
import eu.quickgdx.game.mechanics.entities.TowerType;
import eu.quickgdx.game.mechanics.entities.WaveObject;
import eu.quickgdx.game.mechanics.hud.Beat;
import eu.quickgdx.game.mechanics.hud.HUD;
import eu.quickgdx.game.mechanics.hud.SoundHUD;
import eu.quickgdx.game.screens.GameplayScreen;

/**
 * Created by Veit on 06.02.2016.
 */
public class World {
    public static final float SCALE = 0.8f;
    public static final float TILE_SIZE = 64;
    public static final float SCALED_TILE = SCALE * TILE_SIZE;
    private static final float COOL_DOWN_WAVE = 10;

    private final InputHandler inputHandler;
    public Array<GameObject> gameObjects;
    public GameplayScreen gameplayScreen;
    public HUD hud;
    public SoundHUD soundHud;
    ShapeRenderer sr = new ShapeRenderer();
    PlayerStatusObject playerStatusObject;
    public CreepGoalObject creepGoal;
    public Vector2 startingVectorCreep;
    public Vector2 goalVectorCreep;

    public Array<WaveObject> waveObjects;
    public List<CreepObject> creepObjects;

    //Tiled Map Variables
    private static final String LEVEL1= "level/tower_2.tmx"; //This is your example Tiled Map.
    private static final String LEVEL2 = "level/tower_3.tmx"; //This is your example Tiled Map.
    private static final String LEVEL3 = "level/tower_4.tmx"; //This is your example Tiled Map.
    private static final String LEVEL4 = "level/tower_5.tmx"; //This is your example Tiled Map.

    TiledMap map;
    TiledMapRenderer tiledMapRenderer;
    int mapWidth;
    int tileWidth;
    int mapHeight;
    int tileHeight;
    public boolean buildingModeActive;
    public int buildingsOpenCount;
    float nextWaveTime;


    public TowerType selectedTower = TowerType.UnBuild;
    public float towerPowerMultiplicatinator5000 = 1;
    private boolean waveFinished;

    public World(GameplayScreen gameplayScreen) {
        gameObjects = new Array<GameObject>();
        waveObjects = new Array<WaveObject>();
        creepObjects = new ArrayList<>();
        this.gameplayScreen = gameplayScreen;
        loadTiledMap();

        nextWaveTime = COOL_DOWN_WAVE;
        this.waveFinished = true;
        //Add HUD
        this.playerStatusObject = new PlayerStatusObject(10, nextWaveTime);
        this.hud = new HUD(playerStatusObject, this);
        //Add SoundHUD
        this.soundHud = new SoundHUD(this);
        this.inputHandler = new InputHandler(this);
        this.buildingModeActive = false;
        this.buildingsOpenCount = 0;
    }


    public void update(float delta) {
        nextWaveTime -= delta;
        if (nextWaveTime <= 0 && waveFinished) {
            addWave();
        }
        playerStatusObject.setNextWaveTime(nextWaveTime);
        inputHandler.update(delta);
        for (GameObject go : gameObjects) {
            go.update(delta);
            if (go instanceof CreepObject) {
                if (((CreepObject) go).hitGoal() || ((CreepObject) go).isDead()) {
                    gameObjects.removeValue(go, false);
                    switch (((CreepObject) go).creepType) {
                        case 0:
                            Stats.gothsKilled++;
                            break;
                        case 1:
                            Stats.djsKilled++;
                            break;
                        default:
                            Stats.popsKilled++;
                    }
                    creepObjects.remove(go);
                }
                if (((CreepObject) go).hitGoal()) {
                    playerStatusObject.setHitpoints(playerStatusObject.getHitpoints() - 1);
                    if (playerStatusObject.getHitpoints() <= 0) {
                        this.dispose();
                        gameplayScreen.parentGame.getScreenManager().setCurrentState(ScreenManager.ScreenState.Highscore);
                    }
                }
            }
        }

//        if(creepObjects.size() > 0) {
//            String debugInfo = "Position: " + this.creepObjects.get(0).getPosition().x + " | " + this.creepObjects.get(0).getPosition().y;
//            this.hud.setDebugInfo(debugInfo + "|" + this.creepObjects.get(0).getLastDirection());
//        }

        // add new wave if none has been added before
        if (waveObjects.size <= 0) {
            addWave();
            lastCreepSendAction();
        }

        // run current wave, waveObjetct.size -1 contains next wave
        if (waveObjects.size > 1) {
            waveObjects.get(waveObjects.size - 2).update(delta);
        }
    }

    public void addWave() {
        waveFinished = false;
        WaveObject value = new WaveObject(creepObjects, gameObjects, this);
        waveObjects.add(value);
    }

    public void render(float delta, SpriteBatch spriteBatch) {
        tiledMapRenderer.setView(gameplayScreen.gameCam);
        tiledMapRenderer.render();
        spriteBatch.begin();
        for (GameObject go : gameObjects) {
            go.render(delta, spriteBatch);
        }
        spriteBatch.end();

        //Debug Renderer
        //sr.setProjectionMatrix(gameplayScreen.soundHudCam.combined);
//        sr.setProjectionMatrix(gameplayScreen.gameCam.combined);
//        sr.begin(ShapeRenderer.ShapeType.Line);
//        sr.setColor(0, 1, 0, 1);
//        for (GameObject gameObject : gameObjects) {
//            if (gameObject.getBounds() != null)
//                sr.rect(gameObject.getBounds().x, gameObject.getBounds().y, gameObject.getBounds().width, gameObject.getBounds().height);
//        }
////        sr.circle(soundHud.hitObject.x, soundHud.hitObject.y, soundHud.hitObject.radius);
////        for (Beat beat : soundHud.beats) {
////            sr.circle(beat.x, beat.y, 15);
////        }
//        sr.end();

//        System.out.println("time " + playerStatusObject.getNextWaveTime() + " time " + this.soundHud.newWaveDisplayTime);
        if( playerStatusObject.getNextWaveTime() < 3 && this.soundHud.newWaveDisplayTime <= 0) {
            this.soundHud.notifyNewWave();
        }
    }

    public void renderHUD(float delta, SpriteBatch hudBatch) {
        hudBatch.begin();
        this.hud.render(delta, hudBatch);
        hudBatch.end();
    }

    public void renderSoundHUD(float delta, SpriteBatch soundHudBatch) {
        soundHudBatch.begin();
        this.soundHud.render(delta, soundHudBatch);
        soundHudBatch.end();
    }

    public void touch(Vector3 touchCoords) {
        //controlledObject.touch(touchCoords);
    }

    /**
     * create the map out of the tmx files
     */
    public void loadTiledMap() {
        TmxMapLoader.Parameters params = new TmxMapLoader.Parameters();
        params.textureMinFilter = Texture.TextureFilter.Linear;
        params.textureMagFilter = Texture.TextureFilter.Linear;
        map = new TmxMapLoader().load(getLevelPath());
        tiledMapRenderer = new OrthogonalTiledMapRenderer(map, SCALE); //Just scaled the map - This is not how you should do it obviously!
        mapWidth = map.getProperties().get("width", Integer.class);
        tileWidth = map.getProperties().get("tilewidth", Integer.class);
        mapHeight = map.getProperties().get("height", Integer.class);
        tileHeight = map.getProperties().get("tileheight", Integer.class);

        //load collision map
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("collisionmap");
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if (cell != null) {
                    gameObjects.add(new CollisionObject(new Vector2(x * World.SCALED_TILE, y * World.SCALED_TILE), this, World.SCALED_TILE, World.SCALED_TILE));
                }
            }
        }
        layer = (TiledMapTileLayer) map.getLayers().get("buildmap");
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if (cell != null) {
                    gameObjects.add(new BuildAbleObject(new Vector2(x * World.SCALED_TILE, y * World.SCALED_TILE), this, World.SCALED_TILE * 2, World.SCALED_TILE * 2));
                }
            }
        }
        MapObjects objects = map.getLayers().get("objects").getObjects();
        //create special objects
        for (int i = 0; i < objects.getCount(); i++) {
            MapProperties object = objects.get(i).getProperties();
            String type = object.get("type", String.class);
            float x = object.get("x", Float.class);
            float y = object.get("y", Float.class);
            switch (type) {
                case "start":
                    this.startingVectorCreep = new Vector2(x * SCALE, y * SCALE);
                    break;
                case "goal":
                    creepGoal = new CreepGoalObject(new Vector2(x * SCALE, y * SCALE), this, World.SCALED_TILE, World.SCALED_TILE);
                    break;
            }
        }


    }

    public void addTower(TowerObject towerObject) {
        this.gameObjects.add(towerObject);
    }

    public void dispose(){
        soundHud.dispose();
        WaveObject.waveCount = 0;
    }

    public void lastCreepSendAction() {
        buildingsOpenCount++;
        this.nextWaveTime = COOL_DOWN_WAVE;
        this.waveFinished = true;
    }

    private String getLevelPath(){
        String levelToUse;
        switch (this.gameplayScreen.parentGame.lvlToLoad) {
            case 0:
                levelToUse = LEVEL1;
                break;
            case 1:
                levelToUse = LEVEL2;
                break;
            case 2:
                levelToUse = LEVEL3;
                break;
            default:
                levelToUse = LEVEL4;
        }
        return levelToUse;
    }

}
