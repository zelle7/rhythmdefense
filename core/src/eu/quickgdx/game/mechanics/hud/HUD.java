package eu.quickgdx.game.mechanics.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import eu.quickgdx.game.QuickGdx;
import eu.quickgdx.game.ScreenManager;
import eu.quickgdx.game.mechanics.World;
import eu.quickgdx.game.mechanics.entities.PlayerStatusObject;
import eu.quickgdx.game.mechanics.entities.TowerType;
import eu.quickgdx.game.mechanics.entities.WaveObject;



/**
 * Created by Veit on 23.02.2016.
 */
public class HUD {
    World world;
    PlayerStatusObject playerstatus;
    static BitmapFont textFont;
    static BitmapFont waveTextFont;
    static BitmapFont towerTextFont;
    static BitmapFont hitPointIndicatorFont;
    private GlyphLayout layout = new GlyphLayout();
    private GlyphLayout waveLayout = new GlyphLayout();
    private GlyphLayout towerLayout = new GlyphLayout();
    private GlyphLayout hitpointLayout = new GlyphLayout();
    boolean multiplierUp = false;
    float multiplierTime = 1f;

    Texture hitpointIndicator;
    String debugInfo;

    Array<HUDTower> hudTowers;

    public HUD(PlayerStatusObject object, World world) {
        this.playerstatus = object;
        this.world = world;
        textFont = world.gameplayScreen.parentGame.getAssetManager().get(QuickGdx.FONT_ROBOTO_16_HUD_PATH, BitmapFont.class);
        waveTextFont = world.gameplayScreen.parentGame.getAssetManager().get(QuickGdx.FONT_ROBOTO_16_HUD_PATH, BitmapFont.class);
        towerTextFont = world.gameplayScreen.parentGame.getAssetManager().get(QuickGdx.FONT_ROBOTO_16_HUD_PATH, BitmapFont.class);
        towerTextFont.getData().setScale(0.25f);
        hitPointIndicatorFont = world.gameplayScreen.parentGame.getAssetManager().get(QuickGdx.FONT_BANGERS_HITPOINT_PATH, BitmapFont.class);
//        hitPointIndicatorFont.getData().setScale(0.8f);
        hitpointIndicator = world.gameplayScreen.parentGame.getAssetManager().get("hud/life_small.png");

        hudTowers = new Array<HUDTower>();
        hudTowers.add(new HUDTower(TowerType.Bach, 20, 150, "Bach\nfast", (Texture) world.gameplayScreen.parentGame.getAssetManager().get("towers/bach.png")));
        hudTowers.add(new HUDTower(TowerType.Mozart, 20, 250, "Mozart\nstrong", (Texture) world.gameplayScreen.parentGame.getAssetManager().get("towers/mozart.png")));
        hudTowers.add(new HUDTower(TowerType.Beethoven, 20, 350, "Beethoven\nrange", (Texture) world.gameplayScreen.parentGame.getAssetManager().get("towers/beethoven.png")));
    }


    public void setDebugInfo(String debugInfo) {
        this.debugInfo = debugInfo;
    }

    public void render(float delta, SpriteBatch hudBatch) {
        handleInput();
        //draws the hitpoint indicator
        hudBatch.draw(hitpointIndicator, 10, QuickGdx.GAME_HEIGHT - hitpointIndicator.getHeight() - 20);
        hitpointLayout.setText(hitPointIndicatorFont, playerstatus.getHitpoints()+"x");
        hitPointIndicatorFont.draw(hudBatch, hitpointLayout, 70.0f, QuickGdx.GAME_HEIGHT - hitpointLayout.height - 10);


        //draws the tower selector
        for (int i = 0; i < hudTowers.size; i++){
            HUDTower tower = hudTowers.get(i);
            hudBatch.draw(tower.texture, tower.x, QuickGdx.GAME_HEIGHT - tower.y);
            if(tower.selected) {
                towerTextFont.setColor(255f, 0f, 0f, 255f);
            } else {
                towerTextFont.setColor(255f, 255f, 255f, 255f);
            }
            towerLayout.setText(towerTextFont, tower.name);
            towerTextFont.draw(hudBatch, towerLayout, tower.x, QuickGdx.GAME_HEIGHT - tower.y);
            towerTextFont.setColor(255f, 255f, 255f, 255f);

        }


        //draws debug text
        if (debugInfo != null) {
            layout.setText(textFont, debugInfo);
        }
        textFont.draw(hudBatch, layout, QuickGdx.GAME_WIDTH / 2 - layout.width / 2, QuickGdx.GAME_HEIGHT - layout.height - 650);

        // draws wave information
        WaveObject wave = world.waveObjects.get(world.waveObjects.size - 1);
        String creepType;
        switch (wave.creepType) {
            case 0:
                creepType = "Goth - slow and weak";
                break;
            case 1:
                creepType = "DJ - fast and strong";
                break;
            default:
                creepType = "Mainstreamer - average - ";
        }
//        waveLayout.setText(waveTextFont,
//                "current wave: " + (wave.waveCount - 1) +
//                        "\r\n creep type: " + creepType +
//                        "\r\n next wave: " + wave.waveCount +
//                        "\r\n # Creeps: " + wave.creepAmount +
//                        "\r\n Creep Strength: " + wave.creepStrength +
//                        "\r\n Creep Speed: " + wave.creepSpeed +
//                        "\r\n Time to Spawn: " + (wave.timeBetweenSpawns / 1000f) + " seconds"
//        );
        waveLayout.setText(waveTextFont,
                "musicians to place: " + world.buildingsOpenCount +
                "\r\nnext wave #"+wave.waveCount+": " +
                        wave.creepAmount + "x " +
                        creepType +
                        " in " + playerstatus.getNextWaveTimeFormatted() +"s"
        );
        waveTextFont.draw(hudBatch, waveLayout, 400, QuickGdx.GAME_HEIGHT - 10);

    }

    private void handleInput() {
        //touch
        if (Gdx.input.justTouched()) {
            Vector3 touchWorldCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 1);
            // find the menu item ..
            for (int i = 0; i < hudTowers.size; i++) {
                HUDTower tower = hudTowers.get(i);
                tower.selected = false;
//                world.selectedTower = TowerType.UnBuild;
                if(tower.isClicked(touchWorldCoords)) {
                    world.selectedTower = tower.type;
                }

            }
        }

    }
}
