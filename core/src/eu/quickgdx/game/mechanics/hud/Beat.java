package eu.quickgdx.game.mechanics.hud;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by lknoch on 21.01.17.
 */
public class Beat {

    public boolean isHit = false;
    public float timeStamp;
    public float x;
    public float origx;
    public float y;

    public Beat(float timeStamp, float x, float y) {
        this.timeStamp = timeStamp;
        origx = x;
        this.x = 100000;
        this.y = y;
    }

    public void update(float t){
        x = origx + ((timeStamp -t) * 250);
    }
}
