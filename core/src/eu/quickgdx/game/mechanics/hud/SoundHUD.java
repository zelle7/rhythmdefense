package eu.quickgdx.game.mechanics.hud;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.quickgdx.game.QuickGdx;
import eu.quickgdx.game.mechanics.World;
import eu.quickgdx.game.mechanics.entities.WaveObject;

/**
 * Created by oskar on 20.01.17.
 */

public class SoundHUD implements Music.OnCompletionListener {
    private final Music music;
    private String beet = "thesis";
    World world;
    static BitmapFont textFont;
    private GlyphLayout layout = new GlyphLayout();
    Texture soundwave;
    Texture hitcircle;
    Texture targetcircle;
    Texture hitcirclehit;
    Texture bg;
    Texture powerBarCasing;
    Texture powerBar;
    Texture powerBarBackground;
    float musicStartDelay = 5f;
    public Circle hitObject = new Circle(new Vector2(60,80),40);
    public List<Beat> beats = new ArrayList<>();
    float powerMax = 2;
    private boolean disposed = false;
    private float dampenAgainst = 0.7f;
    public float newWaveDisplayTime;
    private boolean started = false;

    public SoundHUD(World world) {
        this.world = world;
        switch(world.gameplayScreen.parentGame.songToPlay){
            case 0:
                beet = "test.mp3";
                break;
            case 1:
                beet = "thesis.mp3";
                break;
            case 2:
                beet = "129004__hamedpce__1.mp3";
                break;
            case 3:
                beet = "hoerspielwerkstatt-hef__hip-hop-beat-sound-demo.mp3";
                break;

        }
        textFont = world.gameplayScreen.parentGame.getAssetManager().get(QuickGdx.FONT_BANGERS_64_PATH, BitmapFont.class);
        soundwave = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/soundwave.png");
        hitcircle = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/hitcircle.png");
        targetcircle = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/hit_goal_line.png");
        hitcirclehit = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/hitcirclehit.png");
        bg = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/track_bg.png");
        powerBarCasing = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/powerBarCasing.png");
        powerBar = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/powerBar.png");
        powerBarBackground = world.gameplayScreen.parentGame.getAssetManager().get("soundhud/powerBarBackground.png");
        Audio audio = Gdx.audio;
        music = audio.newMusic(Gdx.files.internal("music/" + beet ));
        music.setOnCompletionListener(this);
        loadBeats();
    }

    private void loadBeats() {
        FileHandle handle = Gdx.files.internal("music/" + beet + ".beats");
        String text = handle.readString();
        String wordsArray[] = text.split("\\r?\\n");
        beats.clear();
        for(String word : wordsArray) {
            beats.add(new Beat(Float.parseFloat(word),hitObject.x,hitObject.y));
        }
    }


    public void render(float delta, SpriteBatch soundHudBatch) {
        if(musicStartDelay >0){
            musicStartDelay -= delta;
        }else{
            if(!started){
                music.play();
                started = true;
            }
        }
        soundHudBatch.draw(bg,0,0,Gdx.graphics.getWidth()+500,160);

        updateTimesRelative(delta);
        //draws the hitpoint indicator
        soundHudBatch.draw(targetcircle, hitObject.x- 32, 0, 160/2.5f, 160 );
        renderBeats(soundHudBatch);
        renderPowerBar(soundHudBatch);
        if(newWaveDisplayTime > 0 ){
            newWaveDisplayTime -= delta;
            layout.setText(textFont, "New Wave incoming");
            textFont.draw(soundHudBatch, layout, Gdx.graphics.getHeight()/2, Gdx.graphics.getWidth()/9);
        }
    }

    private void renderPowerBar(SpriteBatch soundHudBatch) {
        int screenParts = QuickGdx.GAME_HEIGHT / 12;
        int powerBarBottomOffset = screenParts * 3;
        int powerBarHeight = screenParts*6;
        int powerBarWidth = powerBarHeight / 3;
        int actualLeftOffset = 20;
        int powerBarLeftOffset = QuickGdx.GAME_WIDTH - actualLeftOffset - powerBarWidth ;
        soundHudBatch.draw(powerBarBackground, powerBarLeftOffset, powerBarBottomOffset, powerBarWidth, powerBarHeight);
        soundHudBatch.draw(powerBar, powerBarLeftOffset, powerBarBottomOffset, powerBarWidth, powerBarHeight *world.towerPowerMultiplicatinator5000/2f,0,0,128, (int) (512 * world.towerPowerMultiplicatinator5000/2f), false, false);
        soundHudBatch.draw(powerBarCasing, powerBarLeftOffset, powerBarBottomOffset, powerBarWidth, powerBarHeight);
    }

    private void renderBeats(SpriteBatch soundHudBatch) {
        //my "now is at #
        for (Beat beat : beats) {
                if(beat.x > Gdx.graphics.getWidth()){ //do not waste time rendering non displayed
                break;
            }
            Texture texture = hitcircle;
            if(beat.isHit){
                texture = hitcirclehit;
            }
            soundHudBatch.draw(texture,beat.x-15, beat.y-15 , 30,30);

        }
    }


    private void updateTimesRelative(float delta) {
        for (Iterator<Beat> iterator = beats.iterator(); iterator.hasNext(); ) {
            Beat beat = iterator.next();
            if(beat.x <= 0){
                iterator.remove();
                decreasePower(true);
            } else{
                if(!music.isPlaying()){
                    beat.update(-musicStartDelay);
                }else{
                    beat.update(music.getPosition());
                }
            }
            if(beat.x >Gdx.graphics.getWidth()){
                break;
            }
        }
        dampen(delta);
    }

    private void dampen(float delta) {
        if(world.towerPowerMultiplicatinator5000 > dampenAgainst){
            float v = delta * ( world.towerPowerMultiplicatinator5000 - dampenAgainst);
            world.towerPowerMultiplicatinator5000 -= v *0.07f;
        }else if(world.towerPowerMultiplicatinator5000 < dampenAgainst){
            float v = delta *  (1-world.towerPowerMultiplicatinator5000);
            world.towerPowerMultiplicatinator5000 += v *0.07f;

        }
    }

    public void tapped() {
        boolean hit = false;
        for (Beat beat: beats  ) {
            if(!beat.isHit && hitObject.contains(beat.x, hitObject.y)){
                beat.isHit = true;
                hit = true;
                break;
            }
        }
        if(hit){
            increasePower();
        }else{
            decreasePower();
        }

    }

    private void decreasePower(boolean lowerBound) {
        float amount = 0.2f;
        if(lowerBound){
            amount = 0.05f;
        }
        world.towerPowerMultiplicatinator5000 -= amount;
        world.towerPowerMultiplicatinator5000 = Math.max(world.towerPowerMultiplicatinator5000,0);

    }
    private void decreasePower() {
        decreasePower(false);
    }

    private void increasePower() {
        world.towerPowerMultiplicatinator5000 += 0.2;
        world.towerPowerMultiplicatinator5000 = Math.min(world.towerPowerMultiplicatinator5000,powerMax);
    }

    public void dispose() {
        music.stop();
        disposed = true;
    }

    @Override
    public void onCompletion(Music music) {
        if(!disposed) {
            music.setPosition(0);
            loadBeats();
            music.play();
        }
    }

    public void notifyNewWave() {
        newWaveDisplayTime = 0.5f;
    }
}
