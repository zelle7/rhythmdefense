package eu.quickgdx.game.mechanics.hud;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

import eu.quickgdx.game.mechanics.entities.TowerType;

/**
 * Created by oskar on 21.01.17.
 */

public class HUDTower {

    public TowerType type;
    public float x, y;
    public String name;
    public Texture texture;
    public boolean selected = false;

    public HUDTower(TowerType type, float x, float y, String name, Texture texture) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.name = name;
        this.texture = texture;
    }

    public boolean isClicked(Vector3 touch){
//        System.out.println("touch: " + touch + " x:" + x  + " x+: " + (x + texture.getWidth())  + " y:" + y  + " y+: " + (y + texture.getHeight()));
        if(touch.x >= x && touch.x <= x + texture.getWidth() && touch.y + texture.getHeight() >= y && touch.y + texture.getHeight() <= y + texture.getHeight()) {
            selected = true;
            return true;
        }
        return false;
    }
}
