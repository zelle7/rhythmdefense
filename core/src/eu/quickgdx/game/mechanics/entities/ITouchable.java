package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by lknoch on 21.01.17.
 */
public interface ITouchable {
    /**
     * indicator if hit
     * @param position
     * @return
     */
    boolean isHit(Vector2 position);

    /**
     * do the hitting stuff here, (check first if hit)
     * @param position
     */
    boolean touch(Vector2 position);
}
