package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import eu.quickgdx.game.mechanics.World;
import eu.quickgdx.game.mechanics.states.BuildingState;
import eu.quickgdx.game.mechanics.states.CoolDownState;
import eu.quickgdx.game.mechanics.states.FireState;
import eu.quickgdx.game.mechanics.states.State;

/**
 * Created by lknoch on 20.01.17.
 */
public class TowerObject extends GameObject implements ITouchable {
    private final static float fireFrameDuration = 0.01f;
    private final Animation buildAnimation;
    private Texture unbuildTexture;
    private Animation fireAnimation;
    private Texture circleTexture;
    private TextureRegion frame;

    private float coolDown; //in seconds, in case you are wondering
    private float attack;
    private float range;
    private boolean isActive;
    private float movingTime;
    private float buildDuration;

    public TowerType towerType = TowerType.UnBuild;
    private boolean selected;


    public TowerObject(Vector2 position, World world, TowerType type) {
        super(position, world);
        this.unbuildTexture = world.gameplayScreen.parentGame.getAssetManager().get("hud/life_small.png");
        this.circleTexture = world.gameplayScreen.parentGame.getAssetManager().get("gameplay/range_indicator.png");
        bounds = new Rectangle(position.x, position.y, World.TILE_SIZE * World.SCALE * 2, World.TILE_SIZE * World.SCALE * 2);
        this.buildAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_Down.png", 0.3f, 45, 64);
        this.selected = false;
        this.towerType = type;
        convertTo(towerType);

    }

    public void convertTo(TowerType type) {


        switch (type) {
            case UnBuild:
                texture = unbuildTexture;
                isActive = false;
                break;
            case Bach:
                texture = world.gameplayScreen.parentGame.getAssetManager().get("towers/bach.png");
                fireAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("towers/fire_bach.png", fireFrameDuration, 64, 64);
                attack = 1;
                isActive = true;
                range = World.SCALED_TILE * 2;
                coolDown = 0.2f;
                buildDuration = 0.5f;
                break;
            case Mozart:
                texture = world.gameplayScreen.parentGame.getAssetManager().get("towers/mozart.png");
                fireAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("towers/fire_mozart.png", fireFrameDuration, 64, 64);
                attack = 10;
                isActive = true;
                range = World.SCALED_TILE * 2;
                coolDown = 0.5f;
                buildDuration = 1;
                break;
            case Beethoven:
                texture = world.gameplayScreen.parentGame.getAssetManager().get("towers/beethoven.png");
                fireAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("towers/fire_beethoven.png", fireFrameDuration, 64, 64);
                attack = 5;
                isActive = true;
                range = World.SCALED_TILE * 4;
                coolDown = 2f;
                buildDuration = 2;
                break;
        }
        if (towerType == TowerType.UnBuild && type != towerType) {
            states.add(new BuildingState(0, this, buildDuration, false));
        }
    }


    @Override
    public void render(float delta, SpriteBatch spriteBatch) {
        if (isBuilding()) {
            TextureRegion frame = buildAnimation.getKeyFrame(movingTime, true);
            spriteBatch.draw(frame, position.x, position.y);
        } else {
            if (selected){
                Color color = spriteBatch.getColor();//get current Color, you can't modify directly
                float oldAlpha = color.a; //save its alpha

                //From here you can modify alpha however you want
                float scale = 0.8f;
                color.a = oldAlpha * scale; //ex. scale = 0.5 will make alpha halved
                spriteBatch.setColor(color); //set it
                spriteBatch.draw(circleTexture, position.x + this.bounds.width / 2 - (range), position.y + this.bounds.height / 2 - range, range*2, range*2);

                //Set it back to orginial alpha when you're done with your alpha manipulation
                color.a = oldAlpha;
                spriteBatch.setColor(color);

            }
            if (fireAnimation != null) {
                if (hasState(FireState.class)) {
                    frame = fireAnimation.getKeyFrame(delta, true);
                } else {
                    frame = fireAnimation.getKeyFrame(0, true);
                }
                spriteBatch.draw(frame, position.x, position.y, bounds.getWidth(), bounds.getHeight());

            }

        }

    }

    @Override
    public boolean isHit(Vector2 position) {

        return bounds.contains(position);
    }

    @Override
    public boolean touch(Vector2 position) {
        boolean hit = isHit(position);
        if (hit) {
            handleTouch();
        }
        this.selected = hit && !isBuilding();
        return hit;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        movingTime += delta;
        if (isActive && !isBuilding() && !isCoolDown()) {
            fire();
        }
    }

    private boolean isBuilding() {
        return hasState(BuildingState.class);
    }

    private boolean hasState(Class stateClass) {
        if (states != null) {
            for (State state : states) {
                if (state.getClass().equals(stateClass)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void fire() {
        //System.out.println("firing from "+position.x +" "+position.y);
        for (CreepObject creepObject : world.creepObjects) {
            if (isWithinRange(creepObject)) {
                hitCreep(creepObject);
                states.add(new CoolDownState(this, coolDown, false));
                break;
            }
        }
    }

    private void hitCreep(CreepObject creepObject) {
//        System.out.println("hit at " + creepObject.position.x + " " + creepObject.position.y);
        creepObject.addHitpoints(-attack * world.towerPowerMultiplicatinator5000);
        addState(new FireState(this, coolDown));
    }

    private boolean isWithinRange(CreepObject creepObject) {
        //one could keep that in memory, but.....
        Circle circle = new Circle(position.x + (bounds.width/2f),position.y + (bounds.width/2f), range);
        return circle.contains(creepObject.getCenter()); //we are shooting on the left foot
    }


    private void handleTouch() {

//        if (towerType == TowerType.UnBuild) {
//            convertTo(TowerType.Bach);
//        } else {
//            convertTo(TowerType.UnBuild);
//        }
    }

    public boolean isCoolDown() {
        return hasState(CoolDownState.class);
    }
}
