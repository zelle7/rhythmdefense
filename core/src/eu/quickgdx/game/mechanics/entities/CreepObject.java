package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import eu.quickgdx.game.mechanics.World;
import eu.quickgdx.game.mechanics.states.HitState;

/**
 * Gives you an simple object controlled by the user
 * Created by Veit on 06.02.2016.
 */
public class CreepObject extends MoveableObject {

    //    private TextureRegion[] regions = new TextureRegion[12];
    private Vector3 touchCoordinates = new Vector3(0, 0, 0);

    private Direction lastDirection;
    private Animation idleAnimation;
    protected Animation movingUpAnimation;
    protected Animation movingDownAnimation;
    protected Animation movingLeftAnimation;
    protected Animation movingRightAnimation;
    protected TextureRegion frame;
    private Texture fireFrame;
    private boolean hitGoal;
    private boolean dmgHit;
    public int creepType;


    public CreepObject(Vector2 position, World world, float speed, float hitpoints, int creepType) {
        super(position, world);
        this.bounds = newPositionRec(position);
        this.speed = speed;
        this.hitpoints = hitpoints;
        float frameDurationAnimation;
        switch (creepType){
            case 0:
                frameDurationAnimation = 0.4f;
                break;
            case 1:
                frameDurationAnimation = 0.2f;
                break;
            case 2:
            default:
                frameDurationAnimation = 0.1f;
                break;
        }
        this.idleAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_creep"+creepType+"_Down.png", frameDurationAnimation, 64, 64);
        this.movingUpAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_creep"+creepType+"_Up.png", frameDurationAnimation, 64, 64);
        this.movingDownAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_creep"+creepType+"_Down.png", frameDurationAnimation, 64, 64);
        this.movingLeftAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_creep"+creepType+"_Left.png", frameDurationAnimation, 64, 64);
        this.movingRightAnimation = world.gameplayScreen.parentGame.getAnimator().loadAnimation("gameplay/movingAnimation_creep"+creepType+"_Right.png", frameDurationAnimation, 64, 64);
        this.fireFrame = world.gameplayScreen.parentGame.getAssetManager().get("gameplay/fire.png");

        this.lastDirection = Direction.DOWN;
        this.hitGoal = false;
        this.dmgHit = false;
        this.creepType = creepType;
    }

    @Override
    public void update(float delta) {
        if (!isDead()) {
            super.update(delta);
            calcMovement();
            handleMovement(delta);
        }

    }

    private void calcMovement() {
        Rectangle newBounds = null;

        Vector2 newPosition = calcNextPosition(this.lastDirection);
        this.directionVector = new Vector2(lastDirection.x * World.TILE_SIZE, lastDirection.y * World.TILE_SIZE);
        newPosition.add(directionVector.nor().scl(speed));
        newBounds = newPositionRec(newPosition);
        boolean collision = collision(newBounds);
        if (collision) {
            if (this.lastDirection.equals(Direction.DOWN) || this.lastDirection.equals(Direction.UP)) {
                this.lastDirection = Direction.LEFT;
                this.directionVector = new Vector2(lastDirection.x * World.TILE_SIZE, lastDirection.y * World.TILE_SIZE);
                newPosition = calcNextPosition(this.lastDirection);
                newPosition.add(directionVector);
                newBounds = newPositionRec(newPosition);
                this.lastDirection = collision(newBounds) ? Direction.RIGHT : Direction.LEFT;

            } else {
                this.lastDirection = Direction.DOWN;
                newPosition = calcNextPosition(this.lastDirection);
                this.directionVector = new Vector2(lastDirection.x * World.TILE_SIZE, lastDirection.y * World.TILE_SIZE);
                newPosition.add(directionVector);
                newBounds = newPositionRec(newPosition);
                this.lastDirection = collision(newBounds) ? Direction.UP : Direction.DOWN;
            }
        }
        setDirection(lastDirection);

        this.bounds = newBounds;
    }

    private Rectangle newPositionRec(Vector2 newPosition) {
        return new Rectangle(newPosition.x, newPosition.y, World.SCALED_TILE - 5, World.SCALED_TILE - 5);
    }


    private boolean collision(Rectangle newBounds) {
        for (int j = 0; j < world.gameObjects.size; j++) {
            GameObject gameObject = world.gameObjects.get(j);
            if (gameObject.bounds != null) {
                if (gameObject.bounds.overlaps(newBounds)) {
                    if (!gameObject.getClass().equals(this.getClass())) {
                        return true;
                    }
                }
            }
        }
        if (world.creepGoal != null && world.creepGoal.bounds.overlaps(newBounds)) {
            this.hitGoal = true;
        } else {
            //System.out.println("no creep goal set!!");
        }
        return false;
    }

    private void setDirection(Direction direc) {
        this.lastDirection = direc;
        this.directionVector = new Vector2(direc.x, direc.y);
    }

    private Vector2 calcNextPosition(Direction direction) {
        return new Vector2(this.position.x + direction.x, this.position.y + direction.y);
    }

    @Override
    void handleMovement(Float delta) {

        this.position.add(directionVector.nor().scl(speed));
        if (!directionVector.nor().isZero()) {
            movement = Movement.MOVING;
        } else {
            movement = Movement.IDLE;
        }
    }


    /**
     * Your typical render function
     *
     * @param delta
     * @param spriteBatch heading must be set accordingly: 1 - UP, 2 - Right, 3 - Down, 4 - Left
     */
    @Override
    public void render(float delta, SpriteBatch spriteBatch) {
        switch (this.lastDirection) {
            case UP:
                frame = movingUpAnimation.getKeyFrame(movingTime, true);
                spriteBatch.draw(frame, position.x, position.y);
                break;
            case LEFT:
                frame = movingLeftAnimation.getKeyFrame(movingTime, true);
                spriteBatch.draw(frame, position.x, position.y);
                break;
            case DOWN:
                frame = movingDownAnimation.getKeyFrame(movingTime, true);
                spriteBatch.draw(frame, position.x, position.y);
                break;
            case RIGHT:
                frame = movingRightAnimation.getKeyFrame(movingTime, true);
                spriteBatch.draw(frame, position.x, position.y);
                break;
            default:
                frame = idleAnimation.getKeyFrame(movingTime, true);
                spriteBatch.draw(frame, position.x, position.y);
        }
        if (this.dmgHit) {
            spriteBatch.draw(fireFrame, position.x, position.y);
        }
    }

    public void touch(Vector3 touchCoordinates) {
        this.touchCoordinates = touchCoordinates;
    }

    public void cameraFollow(Vector2 vector) {
        world.gameplayScreen.gameCam.translate(vector);
    }

    public Direction getLastDirection() {
        return lastDirection;
    }

    public boolean hitGoal() {
        return hitGoal;
    }

    public boolean isDead() {
        return hitpoints <= 0f;
    }

    public void setDmgHit(boolean dmgHit) {
        this.dmgHit = dmgHit;
    }

    /**
     * adds (sub *-1) hitpoints
     *
     * @param hitpoints
     * @return hitpoints after operation
     */
    public float addHitpoints(float hitpoints) {
        this.hitpoints += hitpoints;
        if (hitpoints < 0) {
            addState(new HitState(this));
        }
        return this.hitpoints;
    }

    public boolean isDmgHit() {
        return dmgHit;
    }

    public Vector2 getCenter() {
        return new Vector2(position.x +bounds.width/2f, position.y + bounds.height/2f);
    }

    public enum Direction {
        RIGHT(1, 0), DOWN(0, -1), LEFT(-1, 0), UP(0, 1), IDLE(0, 0);

        private int x;
        private int y;
        private Direction previousDirection;

        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void setPreviousDirection(Direction previousDirection) {
            this.previousDirection = previousDirection;
        }

        public Direction getPreviousDirection() {
            return previousDirection;
        }
    }
}
