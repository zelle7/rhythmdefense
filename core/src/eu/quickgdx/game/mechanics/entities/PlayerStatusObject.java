package eu.quickgdx.game.mechanics.entities;


/**
 * Created by zelle on 20.01.17.
 */

public class PlayerStatusObject {

    private int hitpoints;
    private float nextWaveTime;

    public PlayerStatusObject(int hitpoints, float nextWaveTime) {
        this.hitpoints = hitpoints;
        this.nextWaveTime = nextWaveTime;
    }

    public int getHitpoints() {
        return hitpoints;
    }

    public void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    public float getNextWaveTime() {
        return nextWaveTime;
    }

    public String getNextWaveTimeFormatted() {
        return nextWaveTime >= 0 ? (int) nextWaveTime + "": "0";
    }

    public void setNextWaveTime(float nextWaveTime) {
        this.nextWaveTime = nextWaveTime;
    }
}
