package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.math.Vector2;

import eu.quickgdx.game.mechanics.World;

/**
 * Created by zelle on 20.01.17.
 */
public class CreepGoalObject extends CollisionObject{

    public CreepGoalObject(Vector2 position, World world, float width, float height) {
        super(position, world, width, height);
    }
}
