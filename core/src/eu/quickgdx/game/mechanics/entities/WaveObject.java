package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.graphics.Cursor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.List;

import eu.quickgdx.game.mechanics.World;

/**
 * Created by oskar on 21.01.17.
 */

public class WaveObject {
    public static int waveCount;
    long lastCreepAdded = 0;
    List<CreepObject> creepObjects;
    Array<GameObject> gameObjects;
    World world;

    private int creepCount = 0;

    public long timeBetweenSpawns;
    public int creepAmount;
    public float creepStrength;
    public float creepSpeed;
    public int creepType;


    public WaveObject(List<CreepObject> creepObjects, Array<GameObject> gameObjects, World world) {
        this.creepObjects = creepObjects;
        this.gameObjects = gameObjects;
        this.world = world;
        waveCount++;
//        System.out.println("waveCount: " + waveCount);

        creepType = (int)(Math.random()*3); // 0 = goth, 1 = dj, 2 = pop
//        System.out.println("type: " + type);
        float populationFactor;
        float speedFactor;
        switch (creepType) {
            case 0:
                populationFactor = 2;
                speedFactor = 0.5f;
                break;
            case 1:
                populationFactor = 0.5f;
                speedFactor = 2;
                break;
            default: // = 2
                populationFactor = 1;
                speedFactor = 1;
                break;
        }

        creepAmount = (int) Math.ceil(waveCount*populationFactor);
//        System.out.println("creepAmount: " + creepAmount);

        creepStrength = (waveCount / (float)creepAmount) * 25;
//        System.out.println("creepStrength: " + creepStrength);

        creepSpeed = speedFactor + 2;
//        System.out.println("creepSpeed: " + creepSpeed);

        float spawnDelayLowerBounds = 1f;
        timeBetweenSpawns = (long) Math.max(((1 / speedFactor)- waveCount/10) * 1000, spawnDelayLowerBounds) ;
//        System.out.println("timeBetweenSpawns: " + timeBetweenSpawns);


    }

    public void addNewCreep() {
        CreepObject creepObject = new CreepObject(new Vector2(world.startingVectorCreep.x, world.startingVectorCreep.y), world, creepSpeed, creepStrength, creepType);
        creepObjects.add(creepObject);
        gameObjects.add(creepObject);
        this.lastCreepAdded = System.currentTimeMillis();
        creepCount++;
        if(lastCreepSent()){
            world.lastCreepSendAction();
        }
    }

    public void update(float delta) {
        if(!lastCreepSent()) {
            if ((this.lastCreepAdded + timeBetweenSpawns) < System.currentTimeMillis()) {
                this.addNewCreep();
            }
        }
    }

    public boolean lastCreepSent() {
        return creepCount >= creepAmount;
    }
}
