package eu.quickgdx.game.mechanics.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import eu.quickgdx.game.mechanics.World;

/**
 * Created by zelle on 21.01.17.
 */
public class BuildAbleObject extends GameObject implements ITouchable {

    private Texture buildAbleTexture;
    boolean hasBuilding;
    public BuildAbleObject(Vector2 position, World world, float width, float height) {
        super(position, world);
        this.setBounds(new Rectangle(position.x, position.y, width, height));
        hasBuilding = false;
        buildAbleTexture = world.gameplayScreen.parentGame.getAssetManager().get("gameplay/overlay_active.png");
    }

    @Override
    public void render(float delta, SpriteBatch spriteBatch) {
        if(!hasBuilding && world.selectedTower != TowerType.UnBuild && world.buildingsOpenCount > 0) {
           spriteBatch.draw(buildAbleTexture, position.x, position.y, World.SCALED_TILE * 2, World.SCALED_TILE * 2);
        }
    }

    @Override
    public boolean isHit(Vector2 position) {
        return this.bounds.contains(position);
    }

    @Override
    public boolean touch(Vector2 position) {
        if (isHit(position) && !hasBuilding && world.selectedTower != TowerType.UnBuild && world.buildingsOpenCount > 0) {
            hasBuilding = true;
            world.addTower(new TowerObject(new Vector2(this.position.x, this.position.y), world, world.selectedTower));
            world.buildingsOpenCount--;
            world.selectedTower = TowerType.UnBuild;
        }
        return false;
    }

}
