package eu.quickgdx.game.mechanics.entities;

/**
 * Created by lknoch on 20.01.17.
 */
public enum TowerType {
    UnBuild,
    Bach,
    Mozart,
    Beethoven
}
