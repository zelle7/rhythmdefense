package eu.quickgdx.game.mechanics.states;

import eu.quickgdx.game.mechanics.entities.CreepObject;

/**
 * Created by lknoch on 21.01.17.
 */
public class HitState extends State {

    public HitState(CreepObject parentObject) {
        super(0, parentObject, 0.5f, true);
        this.hasMaxStateTime = true;
        parentObject.setDmgHit(true);
    }

    @Override
    public void remove() {
        super.remove();
        ((CreepObject) parentObject).setDmgHit(false);
    }
}
