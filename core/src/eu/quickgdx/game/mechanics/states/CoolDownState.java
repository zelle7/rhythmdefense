package eu.quickgdx.game.mechanics.states;

import eu.quickgdx.game.mechanics.entities.GameObject;

/**
 * Created by lknoch on 21.01.17.
 */
public class CoolDownState extends State {
    public CoolDownState(GameObject parentObject, float maxStateTime, boolean stackable) {
        super(0, parentObject, maxStateTime, stackable);
    }
}
