package eu.quickgdx.game.mechanics.states;

import eu.quickgdx.game.mechanics.entities.GameObject;

/**
 * Created by Veit on 08.02.2016.
 */
public abstract class State {
    float stateTime;
    GameObject parentObject;
    float maxStateTime;
    boolean hasMaxStateTime;
    public boolean stackable;
    private boolean toRemove;

    public State(float stateTime, GameObject parentObject, float maxStateTime, boolean stackable) {
        this.stateTime = stateTime;
        this.parentObject = parentObject;
        this.maxStateTime = maxStateTime;
        this.hasMaxStateTime = true;
        this.stackable = stackable;
    }

    public State(float stateTime, GameObject parentObject, boolean stackable) {
        this.stateTime = stateTime;
        this.parentObject = parentObject;
        this.hasMaxStateTime = false;
        this.stackable = stackable;
    }

    public void update(float delta) {
        this.stateTime += delta;
        if (this.hasMaxStateTime) {
            if (this.maxStateTime < this.stateTime) {
                this.remove();
            }
        }
    }

    public void remove() {
        toRemove = true;
    }

    public void render() {

    }

    public boolean isToRemove() {
        return toRemove;
    }

    public void setToRemove(boolean toRemove) {
        this.toRemove = toRemove;
    }
}
