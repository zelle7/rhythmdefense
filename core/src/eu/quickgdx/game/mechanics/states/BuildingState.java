package eu.quickgdx.game.mechanics.states;

import eu.quickgdx.game.mechanics.entities.GameObject;

/**
 * Created by lknoch on 21.01.17.
 */
public class BuildingState extends State {
    public BuildingState(float stateTime, GameObject parentObject, float maxStateTime, boolean stackable) {
        super(stateTime, parentObject, maxStateTime, stackable);
        this.hasMaxStateTime = true;
    }
}
