package eu.quickgdx.game.mechanics.states;

import eu.quickgdx.game.mechanics.entities.CreepObject;
import eu.quickgdx.game.mechanics.entities.TowerObject;

/**
 * Created by lknoch on 21.01.17.
 */
public class FireState extends State {

    public FireState(TowerObject towerObject, float coolDown) {
        super(0, towerObject, coolDown/10f, true);
    }

    @Override
    public void remove() {
        super.remove();
    }
}
