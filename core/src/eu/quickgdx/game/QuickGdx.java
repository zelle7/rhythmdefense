package eu.quickgdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Main entry point in our game. Asset loading should be done here.
 * provides you with the basic manager classes (assets, screen, sound) and the animator helper class
 */
public class QuickGdx extends ApplicationAdapter {
    public static final String FONT_ROBOTO_16_HUD_PATH = "fonts/roboto_regular_hud.fnt";
    public static final String FONT_ROBOTO_16_PATH = "fonts/roboto_regular_16.fnt";
    public static final String FONT_ROBOTO_32_PATH = "fonts/roboto_regular_32.fnt";
    public static final String FONT_ROBOTO_64_PATH = "fonts/roboto_regular_64.fnt";
    public static final String FONT_BANGERS_32_PATH = "fonts/bangers_32.fnt";
    public static final String FONT_BANGERS_64_PATH = "fonts/bangers_64.fnt";
    public static final String FONT_BANGERS_HITPOINT_PATH = "fonts/bangers_hitpoints.fnt";

    private SpriteBatch batch;
    private AssetManager assMan;
    private ScreenManager screenManager;
    private SoundManager soundManager;
    private Animator animator;

    // gives the original size for all screen working with the scaling orthographic camera
    // set in DesktopLauncher to any resolution and it will be scaled automatically.
    public static final int GAME_WIDTH = 1366;
    public static final int GAME_HEIGHT = 768;
    public int lvlToLoad;
    public int songToPlay; //usen in SoundHUD


    @Override
    public void create() {
        screenManager = new ScreenManager(this);
        soundManager = new SoundManager(this);
        animator = new Animator(this);

        // LOAD ASSETS HERE ...
        // Loading screen will last until the last one is loaded.
        assMan = new AssetManager();
        // for the menu
        assMan.load("menu/menu_background.jpg", Texture.class);
        assMan.load("menu/title_screen_background.png", Texture.class);
        assMan.load("menu/title_screen_tower_and_banner.png", Texture.class);
        // for the credits
        assMan.load("credits/gradient_top.png", Texture.class);
        assMan.load("credits/gradient_bottom.png", Texture.class);
        // for the sounds
        assMan.load("sfx/blip.wav", Sound.class);
        assMan.load("sfx/explosion.wav", Sound.class);
        assMan.load("sfx/hit.wav", Sound.class);
        assMan.load("sfx/jump.wav", Sound.class);
        assMan.load("sfx/laser.wav", Sound.class);
        assMan.load("sfx/pickup.wav", Sound.class);
        assMan.load("sfx/powerup.wav", Sound.class);
        assMan.load("music/test.mp3", Sound.class);

        //HUD
        assMan.load("hud/life_small.png", Texture.class);

        //towers
        assMan.load("towers/mozart.png", Texture.class);
        assMan.load("towers/beethoven.png", Texture.class);
        assMan.load("towers/bach.png", Texture.class);

        //SoundHUD
        assMan.load("soundhud/note.png", Texture.class);
        assMan.load("soundhud/soundwave.png", Texture.class);
        assMan.load("soundhud/hitcircle.png", Texture.class);
        assMan.load("soundhud/hitcirclehit.png", Texture.class);
        assMan.load("soundhud/hit_goal_line.png", Texture.class);
        assMan.load("soundhud/track_bg.png", Texture.class);
        assMan.load("soundhud/powerBarCasing.png", Texture.class);
        assMan.load("soundhud/powerBar.png", Texture.class);
        assMan.load("soundhud/powerBarBackground.png", Texture.class);

        //Fonts
        assMan.load(FONT_ROBOTO_16_PATH, BitmapFont.class);
        assMan.load(FONT_ROBOTO_32_PATH, BitmapFont.class);
        assMan.load(FONT_ROBOTO_64_PATH, BitmapFont.class);
        assMan.load(FONT_ROBOTO_16_HUD_PATH, BitmapFont.class);
        assMan.load(FONT_BANGERS_32_PATH, BitmapFont.class);
        assMan.load(FONT_BANGERS_64_PATH, BitmapFont.class);
        assMan.load(FONT_BANGERS_HITPOINT_PATH, BitmapFont.class);



        //Entities
        assMan.load("gameplay/spritesheet.png", Texture.class);
        assMan.load("gameplay/movingAnimation_Down.png", Texture.class);
        assMan.load("gameplay/fire.png", Texture.class);
        assMan.load("gameplay/tower_mozart.png", Texture.class);
        assMan.load("gameplay/overlay_active.png", Texture.class);
        assMan.load("gameplay/range_indicator.png", Texture.class);
        for (int i = 0; i < 3; i++) {
            assMan.load("gameplay/movingAnimation_creep"+i+"_Down.png", Texture.class);
            assMan.load("gameplay/movingAnimation_creep"+i+"_Up.png", Texture.class);
            assMan.load("gameplay/movingAnimation_creep"+i+"_Left.png", Texture.class);
            assMan.load("gameplay/movingAnimation_creep"+i+"_Right.png", Texture.class);
        }

        assMan.load("towers/fire_bach.png", Texture.class);
        assMan.load("towers/fire_mozart.png", Texture.class);
        assMan.load("towers/fire_beethoven.png", Texture.class);

    }

    @Override
    public void render() {
        screenManager.getCurrentScreen().render(Gdx.graphics.getDeltaTime());
    }

    public AssetManager getAssetManager() {
        return assMan;
    }

    public ScreenManager getScreenManager() {
        return screenManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public Animator getAnimator() {
        return animator;
    }
}
