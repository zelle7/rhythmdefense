package eu.quickgdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import eu.quickgdx.game.ScreenManager;
import eu.quickgdx.game.mechanics.World;
import eu.quickgdx.game.QuickGdx;
import eu.quickgdx.game.mechanics.entities.WaveObject;

/**
 * Created by Mathias Lux, mathias@juggle.at,  on 04.02.2016.
 */
public class GameplayScreen extends ScreenAdapter {



    private final SpriteBatch batch;
    private final SpriteBatch gameBatch;
    private final SpriteBatch hudBatch;
    private final SpriteBatch soundHudBatch;
    public final OrthographicCamera gameCam;
    public final OrthographicCamera hudCam;
    public final OrthographicCamera soundHudCam;
    private final OrthographicCamera cam;
    public QuickGdx parentGame;

    Texture backgroundImage;
    BitmapFont menuFont;
    World world;


    float offsetLeft = QuickGdx.GAME_WIDTH / 8, offsetTop = QuickGdx.GAME_WIDTH / 8, offsetY = QuickGdx.GAME_HEIGHT / 8;


    public GameplayScreen(QuickGdx game) {
        this.parentGame = game;
        backgroundImage = parentGame.getAssetManager().get("menu/menu_background.jpg");
        menuFont = parentGame.getAssetManager().get(QuickGdx.FONT_ROBOTO_64_PATH);
        menuFont.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        gameCam = new OrthographicCamera(QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        gameCam.position.set(gameCam.viewportWidth / 2f, gameCam.viewportHeight / 2f, 0);
        gameCam.translate(Gdx.graphics.getWidth() / 12, -Gdx.graphics.getWidth() / 16);
        gameCam.zoom += 0.4f;
        gameCam.update();
        hudCam = new OrthographicCamera(QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        hudCam.position.set(hudCam.viewportWidth / 2f, hudCam.viewportHeight / 2f, 0);
        hudCam.update();
        soundHudCam = new OrthographicCamera(QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        soundHudCam.position.set(soundHudCam.viewportWidth / 2f, soundHudCam.viewportHeight / 2f, 0);
        soundHudCam.update();
        gameBatch = new SpriteBatch();
        hudBatch = new SpriteBatch();
        soundHudBatch = new SpriteBatch();
        this.world = new World(this);

        // Create camera that projects the game onto the actual screen size.
        cam = new OrthographicCamera(QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
        cam.update();

        batch = new SpriteBatch();
        backgroundImage = parentGame.getAssetManager().get("menu/title_screen_background.png");
    }

    @Override
    public void render(float delta) {
        // camera:
        cam.update();
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        // draw bgImage
        batch.draw(backgroundImage, 0, 0, QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        batch.end();

        handleInput();
        gameBatch.setProjectionMatrix(gameCam.combined);
        hudBatch.setProjectionMatrix(hudCam.combined);
        soundHudBatch.setProjectionMatrix(soundHudCam.combined);
        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.update(delta);
        world.render(delta, gameBatch);
        world.renderHUD(delta, hudBatch);
        world.renderSoundHUD(delta, soundHudBatch);
        gameCam.update();
        hudCam.update();
        soundHudCam.update();
    }

    private void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            parentGame.getSoundManager().playEvent("explode");
            world.dispose();
            parentGame.getScreenManager().setCurrentState((ScreenManager.ScreenState.Menu));
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.N)) {
            world.addWave();

        }
    }



}
