package eu.quickgdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import eu.quickgdx.game.QuickGdx;
import eu.quickgdx.game.ScreenManager;


public class LevelScreen extends ScreenAdapter {
    private final SpriteBatch batch;
    private final OrthographicCamera cam;
    private QuickGdx parentGame;

    Texture backgroundImage;
    BitmapFont menuFont;

    String[] levelStrings = {"Level 1", "Level 2", "Level 3", "Level 4"};
    String[] songStrings = {"nr. 9", "thesis", "dub", "hip hop"};
    int currentLevel = 0;
    int currentSong = 0;

    float offsetLeft = QuickGdx.GAME_WIDTH / 10, offsetTop = QuickGdx.GAME_WIDTH / 8, offsetY = QuickGdx.GAME_HEIGHT / 8, offsetSongs = (QuickGdx.GAME_WIDTH / 10f)*5 ;
    private float offsetStartLeft = (QuickGdx.GAME_WIDTH / 4) * 3;
    private float offsetBottom = 0;


    public LevelScreen(QuickGdx game) {
        this.parentGame = game;

        backgroundImage = parentGame.getAssetManager().get("menu/title_screen_background.png");
        menuFont = parentGame.getAssetManager().get(QuickGdx.FONT_BANGERS_64_PATH);
        menuFont.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        // Create camera that projects the game onto the actual screen size.
        cam = new OrthographicCamera(QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);

        cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
        cam.update();

        batch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        handleInput();
        // camera:
        cam.update();
        batch.setProjectionMatrix(cam.combined);


        Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        // draw bgImage
        batch.draw(backgroundImage, 0, 0, QuickGdx.GAME_WIDTH, QuickGdx.GAME_HEIGHT);
        // draw Strings ...
        for (int i = 0; i < levelStrings.length; i++) {
            if (i == currentLevel) menuFont.setColor(0.2f, 1f, 0.2f, 1f);
            else menuFont.setColor(0.2f, 0.2f, 1f, 1f);
            menuFont.draw(batch, levelStrings[i], offsetLeft, QuickGdx.GAME_HEIGHT - offsetTop - i * offsetY);
        }

        for (int i = 0; i < songStrings.length; i++) {
            if (i == currentSong) menuFont.setColor(0.2f, 1f, 0.2f, 1f);
            else menuFont.setColor(0.2f, 0.2f, 1f, 1f);
            menuFont.draw(batch, songStrings[i], offsetSongs, QuickGdx.GAME_HEIGHT - offsetTop - i * offsetY);
        }
        menuFont.draw(batch, "Start", offsetStartLeft, offsetBottom + menuFont.getLineHeight());
        batch.end();
    }

    private void handleInput() {
        // keys ...
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            currentLevel = (currentLevel + 1) % levelStrings.length;
            parentGame.getSoundManager().playEvent("blip");
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
//            currentLevel = Math.floorMod((currentLevel - 1), levelStrings.length); // only available in Java 1.8
            currentLevel = (currentLevel - 1) % levelStrings.length;
            parentGame.getSoundManager().playEvent("blip");
        } else
        // touch
        if (Gdx.input.justTouched()) {
            Vector3 touchWorldCoords = cam.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 1));
            // find the menu item ..
            for (int i = 0; i < levelStrings.length; i++) {
                if (touchWorldCoords.x > offsetLeft && touchWorldCoords.x < offsetSongs) {
                    float pos = QuickGdx.GAME_HEIGHT - offsetTop - i * offsetY;
                    if (touchWorldCoords.y < pos && touchWorldCoords.y > pos - menuFont.getLineHeight()) {
                        currentLevel = i;
                    }
                }
            }
                for (int i = 0; i < songStrings.length; i++) {
                    if (touchWorldCoords.x > offsetSongs) {
                        float pos = QuickGdx.GAME_HEIGHT - offsetTop - i * offsetY;
                        if (touchWorldCoords.y < pos && touchWorldCoords.y > pos-menuFont.getLineHeight()) {
                            currentSong = i;
                        }
                    }

            }

            if(touchWorldCoords.x > offsetStartLeft && touchWorldCoords.y < offsetBottom + menuFont.getLineHeight()){
                parentGame.lvlToLoad = currentLevel;
                parentGame.songToPlay = currentSong;
                parentGame.getScreenManager().setCurrentState(ScreenManager.ScreenState.Game);
            }
        }


    }


}
